﻿'' Primary database link object.

Option Explicit On

Imports System.Text
Imports MySql.Data.MySqlClient

''' <summary>
''' EventArgs-derived class used to raise OpenLinkResult events.
''' </summary>
''' <remarks></remarks>
Public Class OpenLinkResultEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Indicates that the link was successfully opened.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Connected As Boolean

    ''' <summary>
    ''' If the connection failed, returns the error message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ErrorMessage As String

    ''' <summary>
    ''' Create a new OpenLinkResultEventArgs object
    ''' </summary>
    ''' <param name="c">Connection result</param>
    ''' <param name="e">Optional error message</param>
    ''' <remarks></remarks>
    Friend Sub New(c As Boolean, Optional e As String = Nothing)
        Connected = c
        ErrorMessage = e
    End Sub

End Class

''' <summary>
''' The prime mover of the database communications infrastructure of SealedBySanta POMS
''' </summary>
''' <remarks></remarks>
Public Class LinkInterface

    Public Event Disconnected(sender As Object, e As EventArgs)
    Public Event OpenLinkResult(sender As Object, e As OpenLinkResultEventArgs)

    Private WithEvents _Link As MySqlConnection

    Private _DefaultConnStr As String

    Private _Connected As Boolean = False

    'Private _CSV As New CSVReader

    Private _Results As MySqlResult

    Private _MaxTries = 10

    Private _LastError As Integer

    Private _LastErrorMessage As String

    Private _LastQueryOk As Boolean


#Region "Shared Functions"

    ''' <summary>
    ''' Escape a string for passing to MySql
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="StripLF"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function EscapeString(ByVal s As String, Optional ByVal StripLF As Boolean = False) As String

        Dim o As String

        If (s Is Nothing) Then Return ""

        o = s.Replace("\", "\\")
        o = o.Replace(Chr(9), "\t")
        o = o.Replace("'", "''")

        o = o.Replace(vbCr, "\r")

        If (StripLF = True) Then
            o = o.Replace(vbLf, "\n")
        Else
            o = o.Replace(vbLf, "\n")

        End If
        o = o.Replace(Chr(34), "\" + Chr(34))

        Return o

    End Function

#End Region

#Region "Public Properties"

    ''' <summary>
    ''' Gets or sets a value indicating whether the database link is open.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Connected() As Boolean
        Get
            Return _Connected
        End Get
        Set(ByVal value As Boolean)

            If (_Connected <> value) Then
                If (value = False) Then
                    Me.CloseLink()
                Else
                    Me.OpenLink()
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the name of the schema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Database() As String = "pillsafe"

    ''' <summary>
    ''' Gets or sets the name of the SQL host
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Host() As String = "localhost"

    ''' <summary>
    ''' Gets or sets the name of the SQL user
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property User() As String = "root"

    ''' <summary>
    ''' Gets or sets the login password
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Password() As String = "Moshka44!"

    ''' <summary>
    ''' Gets or sets a value indicating whether to automatically disconnect after every query.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CloseAfterQuery As Boolean = True

    ''' <summary>
    ''' Returns the last query result.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Result() As MySqlResult
        Get
            Return _Results
        End Get
    End Property

#End Region

#Region "Link Control"

    ''' <summary>
    ''' Opens the link.
    ''' </summary>
    ''' <remarks></remarks>
    Public Function OpenLink() As Boolean

        _Link = New MySqlConnection()

        _DefaultConnStr = "Connection Timeout=60;Default Command Timeout=60;server=" + Host + ";user id=" + User + ";Password=" + Password + ";database=" + Database
        _Link.ConnectionString = _DefaultConnStr

        Dim i As Integer = 0

        For i = 0 To 5
            Try
                _Link.Open()
                Exit For

            Catch ex As Exception

                _LastErrorMessage = ex.Message
                _LastError = ex.HResult
                Try
                    _Link.Close()
                Catch ex2 As Exception

                End Try
                Continue For
            End Try

        Next

        If i > 5 Then
            MsgBox(_LastErrorMessage)
            Return False
        End If

        'Dim c As MySqlCommand = _Link.CreateCommand
        'c.CommandType = CommandType.Text
        'c.CommandText = "SET CHARACTER SET 'utf8';"

        'c.ExecuteNonQuery()

        'c.CommandType = CommandType.Text
        'c.CommandText = "SET NAMES 'utf8' COLLATE 'utf8_unicode_ci';"

        'c.ExecuteNonQuery()

        Return True

    End Function

    ''' <summary>
    ''' Attempt to reopen the link, same as calling CloseLink followed by OpenLink
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReopenLink()

        Try
            Me.CloseLink()
        Catch ex As Exception

        End Try
        System.Threading.Thread.Sleep(0)
        System.Threading.Thread.Sleep(0)

        Me.OpenLink()
        System.Threading.Thread.Sleep(0)
        System.Threading.Thread.Sleep(0)

    End Sub

    ''' <summary>
    ''' Close the link to the current connection.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CloseLink()

        If _Link Is Nothing Then Return

        Try
            _Link.Close()
        Catch ex As Exception

        Finally
            _Link = Nothing
        End Try

    End Sub

    Private Sub _Link_StateChange(ByVal sender As Object, ByVal e As System.Data.StateChangeEventArgs) Handles _Link.StateChange

        If (e.CurrentState = ConnectionState.Closed) Then
            _Connected = False
            If (e.OriginalState = ConnectionState.Closed) Then
                RaiseEvent OpenLinkResult(Me, New OpenLinkResultEventArgs(False, "Connection Failed. Check Settings."))
            Else
                RaiseEvent Disconnected(Me, New EventArgs)
            End If

        ElseIf (e.CurrentState = ConnectionState.Open) Then
            If (_Connected = False) Then
                _Connected = True
                RaiseEvent OpenLinkResult(Me, New OpenLinkResultEventArgs(True, "Connected."))
            End If

        End If

    End Sub

#End Region

#Region "Link Status"

    ''' <summary>
    ''' Returns the state of the current link.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ConnectionStatus As ConnectionState
        Get
            Return _Link.State
        End Get
    End Property

    ''' <summary>
    ''' Returns the last error code (HResult) from a failed query or execution.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LastError As Integer
        Get
            Return _LastError
        End Get
    End Property

    ''' <summary>
    ''' Returns the last error message from the a failed query or execution.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LastErrorMessage As String
        Get
            Return _LastErrorMessage
        End Get
    End Property

    ''' <summary>
    ''' Indicates whether or not the last query or execution succeeded.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LastQueryOk As Boolean
        Get
            Return _LastQueryOk
        End Get
    End Property

#End Region

#Region "Database Functions"

    Private Sub setError(queryok As Boolean, Optional code As Integer = 0, Optional text As String = "OK")
        _LastError = code
        _LastErrorMessage = text
        _LastQueryOk = queryok

    End Sub

    ''' <summary>
    ''' Returns true if the link exists and is open.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsLinkOpen As Boolean
        Get
            Return _Link IsNot Nothing AndAlso _Link.State = ConnectionState.Open
        End Get
    End Property

    ''' <summary>
    ''' Execute a query or stored procedure
    ''' </summary>
    ''' <param name="req">Query string</param>
    ''' <param name="cmdType">Command type</param>
    ''' <param name="params">Parameters</param>
    ''' <param name="failNoRows">Fail if no rows are affected by the call.</param>
    ''' <returns>A Raw XRecord(,)</returns>
    ''' <remarks></remarks>
    Public Function Request(ByVal req As String, Optional ByVal cmdType As CommandType = CommandType.Text, Optional params() As MySqlParameter = Nothing, Optional failNoRows As Boolean = False) As XRecord(,)

        Dim c As MySql.Data.MySqlClient.MySqlCommand
        Dim r As MySql.Data.MySqlClient.MySqlDataReader
        Dim i, j, l, m, v
        Dim ct As CommandType = cmdType

        Dim kvp(,) As XRecord = Nothing,
            bx() As XRecord,
            fp(,) As XRecord,
            nx() As String = Nothing

        Dim rt As Integer = 0

rqAgain:

        If Not IsLinkOpen Then OpenLink()

        Try
            c = _Link.CreateCommand()

            c.Parameters.Clear()
            c.CommandType = ct
            c.CommandText = req

            If params IsNot Nothing Then
                For Each s As MySqlParameter In params
                    c.Parameters.Add(s)
                Next
            End If

            r = c.ExecuteReader()
            If r.HasRows = False AndAlso failNoRows = True Then
                setError(False, 0, "NoResults")

                Return Nothing
            End If
        Catch ex As Exception
            If (rt >= _MaxTries) Then

                setError(False, ex.HResult, ex.Message)

                Return Nothing
            End If

            rt += 1
            _Link = New MySqlConnection()
            System.Threading.Thread.Sleep(0)

            GoTo rqAgain

        End Try

        i = r.VisibleFieldCount
        l = 0
        m = r.Read()

        For j = 0 To i - 1
            ReDim Preserve nx(j)
            nx(j) = r.GetName(j)

        Next

        Dim ox As Object()

        Dim rll As New List(Of Object())

        While m = True
            ReDim ox(i - 1)
            r.GetValues(ox)
            rll.Add(ox)
            m = r.Read()
        End While

        ReDim kvp(i - 1, rll.Count - 1)
        ReDim bx(i - 1)

        l = 0

        For Each ox In rll

            v = Nothing
            For j = 0 To i - 1
                v = ox(j)

                If TypeOf v Is System.Array Then

                    If (v IsNot Nothing) AndAlso (Not TypeOf v Is DBNull) Then
                        bx(j).Value = System.Text.Encoding.ASCII.GetString(v)
                    Else
                        bx(j).Value = Nothing
                    End If

                    bx(j).Key = nx(j)
                    kvp(j, l) = bx(j)
                Else

                    If (v IsNot Nothing) AndAlso (Not TypeOf v Is DBNull) Then
                        bx(j).Value = v
                    Else
                        bx(j).Value = Nothing
                    End If

                    bx(j).Key = nx(j)
                    kvp(j, l) = bx(j)
                End If
            Next
            l += 1
        Next

        ReDim fp(l - 1, i - 1)

        For j = 0 To i - 1
            For m = 0 To l - 1
                fp(m, j) = kvp(j, m)
            Next
        Next

        c.Dispose()
        c = Nothing
        r.Close()

        _Results = New MySqlResult(fp)
        System.Threading.Thread.Sleep(0)

        If CloseAfterQuery Then CloseLink()

        _LastError = 0
        _LastErrorMessage = "OK"
        _LastQueryOk = True

        Return fp
    End Function

    ''' <summary>
    ''' Insert a row into the specified table
    ''' </summary>
    ''' <param name="Table">Table to insert row</param>
    ''' <param name="Data">MySqlRecord to insert</param>
    ''' <param name="Replace">Replace the entire contents of the record if it already exists.</param>
    ''' <returns>The insert id of the new record</returns>
    ''' <remarks></remarks>
    Public Overloads Function InsertRow(ByVal Table As String, ByVal Data As MySqlRecord, Optional ByVal Replace As Boolean = True) As Integer

        Dim t() As XRecord

        t = Data.GetXRecord()
        Return InsertRow(Table, t, Replace)

    End Function

    ''' <summary>
    ''' Insert a row into the specified table
    ''' </summary>
    ''' <param name="Table">Table to insert row</param>
    ''' <param name="Data">Key/Value pair array to insert</param>
    ''' <param name="Replace">Replace the entire content of the record if the record already exists.</param>
    ''' <returns>The insert id of the new record</returns>
    ''' <remarks></remarks>
    Public Overloads Function InsertRow(ByVal Table As String, ByVal Data() As XRecord, Optional ByVal Replace As Boolean = False) As Integer
        '// data is an associative array (field => value)
        '// inserts a row in the given table.

        '// if $ignore=TRUE, disregard whether row already exists, or not.
        '// unless otherwise stated, strip all HTML tags for security and triumph over stupidity.

        Dim rt As Integer
        Dim q As String
        Dim r As Integer
        Dim c As MySqlCommand

        System.Threading.Thread.Sleep(0)

        q = GetInsertString(Table, Data, Replace)

insAgain:

        If (Not IsLinkOpen) Then
            Me.OpenLink()
            System.Threading.Thread.Sleep(0)
        End If

        Try
            c = _Link.CreateCommand()

            c.CommandType = CommandType.Text
            c.CommandText = q

            r = c.ExecuteNonQuery()

        Catch ex As Exception
            If (rt >= _MaxTries) Then
                setError(False, ex.HResult, ex.Message)

                If CloseAfterQuery Then CloseLink()
                Return 0
            End If

            _Link = New MySqlConnection()

            rt += 1
            System.Threading.Thread.Sleep(0)

            GoTo insAgain

        End Try

        If CloseAfterQuery Then CloseLink()

        If (r >= 1) Then
            Return c.LastInsertedId
        End If

        Return 0

    End Function

    ''' <summary>
    ''' Executes a series of inserts/replaces in a single query.
    ''' </summary>
    ''' <param name="Table"></param>
    ''' <param name="Data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertMultipleRows(Table As String, Data As MySqlResult, Optional Replace As Boolean = False) As Integer
        Dim sb As New StringBuilder

        Dim i As Integer,
            d As Integer = Data.Count - 1

        Dim r As Integer,
            rt As Integer,
            c As MySqlCommand

        Dim q As String

        For i = 0 To d
            sb.Append(GetInsertString(Table, Data(i).GetXRecord, Replace))
        Next

        q = sb.ToString

insAgain:

        If (Not IsLinkOpen) Then
            Me.OpenLink()
            System.Threading.Thread.Sleep(0)
        End If

        Try
            c = _Link.CreateCommand()

            c.CommandType = CommandType.Text
            c.CommandText = q

            r = c.ExecuteNonQuery()

        Catch ex As Exception
            If (rt >= _MaxTries) Then
                setError(False, ex.HResult, ex.Message)

                If CloseAfterQuery Then CloseLink()
                Return 0
            End If

            _Link = New MySqlConnection()

            rt += 1
            System.Threading.Thread.Sleep(0)

            GoTo insAgain

        End Try

        If CloseAfterQuery Then CloseLink()

        If (r >= 1) Then
            Return c.LastInsertedId
        End If

        Return 0

    End Function

    ''' <summary>
    ''' Modifies a given set of rows in a table using a primary key.
    ''' </summary>
    ''' <param name="Table">Table to modify.</param>
    ''' <param name="Key">Primary Key.</param>
    ''' <param name="Data">Modified data.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function ModifyRows(ByVal Table As String, ByVal Key As MySqlRecord, ByVal Data As MySqlRecord) As Integer
        Dim l As New List(Of String)

        '' Let's make sure we're not going to be replacing the keys!

        For Each m In Key
            Data.Remove(m.Key)
        Next

        Dim t() As XRecord = Data.GetXRecord
        Return ModifyRows(Table, GetKeyString(Key), t)

    End Function

    ''' <summary>
    ''' Executes a series of modifications in a single query.
    ''' </summary>
    ''' <param name="Table"></param>
    ''' <param name="Keys"></param>
    ''' <param name="Data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ModifyMultipleRows(Table As String, Keys As MySqlResult, Data As MySqlResult) As Integer
        Dim sb As New StringBuilder

        If Keys Is Nothing OrElse Data Is Nothing OrElse Keys.Count <> Data.Count Then Return 0

        Dim i As Integer,
            d As Integer = Keys.Count - 1

        Dim r As Integer,
            rt As Integer,
            c As MySqlCommand

        Dim q As String

        For i = 0 To d
            For Each m In Keys(i)
                Data(i).Remove(m.Key)
            Next
            sb.Append(GetModifyString(Table, GetKeyString(Keys(i)), Data(i).GetXRecord))
        Next

        q = sb.ToString

insAgain:

        If (Not IsLinkOpen) Then
            Me.OpenLink()
            System.Threading.Thread.Sleep(0)
        End If

        Try
            c = _Link.CreateCommand()

            c.CommandType = CommandType.Text
            c.CommandText = q

            r = c.ExecuteNonQuery()

        Catch ex As Exception
            If (rt >= _MaxTries) Then
                setError(False, ex.HResult, ex.Message)

                If CloseAfterQuery Then CloseLink()
                Return 0
            End If

            _Link = New MySqlConnection()

            rt += 1
            System.Threading.Thread.Sleep(0)

            GoTo insAgain

        End Try

        If CloseAfterQuery Then CloseLink()

        If (r >= 1) Then
            Return c.LastInsertedId
        End If

        Return 0

    End Function

    ''' <summary>
    ''' Modifies a given set of rows in a table.
    ''' </summary>
    ''' <param name="Table">Table to modify.</param>
    ''' <param name="Match">Match condition.</param>
    ''' <param name="Data">Modified data.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function ModifyRows(ByVal Table As String, ByVal Match As String, ByVal Data As MySqlRecord) As Integer

        Dim t() As XRecord
        t = Data.GetXRecord()

        Return ModifyRows(Table, Match, t)

    End Function

    ''' <summary>
    ''' Modifies a given set of rows in a table.
    ''' </summary>
    ''' <param name="Table">Table to modify.</param>
    ''' <param name="Match">Match condition.</param>
    ''' <param name="Data">Modified data.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function ModifyRows(ByVal Table As String, ByVal Match As String, ByVal Data() As XRecord) As Integer
        ' match is a "WHERE" query string part (without the "WHERE")

        ' fields is a 2 dimensional array (data field => new value)


        Dim rt As Integer

        Dim q As String
        Dim r As Integer

        Dim c As MySqlCommand

        q = GetModifyString(Table, Match, Data)

modAgain:

        System.Threading.Thread.Sleep(0)

        If (Not IsLinkOpen) Then
            Me.OpenLink()
            System.Threading.Thread.Sleep(0)
        End If

        Try
            c = _Link.CreateCommand()

            c.CommandType = CommandType.Text
            c.CommandText = q

            r = c.ExecuteNonQuery()

        Catch ex As Exception
            If (rt >= _MaxTries) Then
                setError(False, ex.HResult, ex.Message)

                If CloseAfterQuery Then CloseLink()
                Return 0
            End If

            _Link = New MySqlConnection()

            rt += 1
            System.Threading.Thread.Sleep(0)

            GoTo modAgain

        End Try

        If CloseAfterQuery Then CloseLink()

        If (r >= 1) Then
            Return r
        End If

        Return 0

    End Function

    ''' <summary>
    ''' Deletes a given set of rows in a table.
    ''' </summary>
    ''' <param name="Table">Table to modify.</param>
    ''' <param name="Match">Match condition.</param>
    ''' <returns>The number of rows affected</returns>
    ''' <remarks></remarks>
    Public Overloads Function DeleteRows(ByVal Table As String, ByVal Match As String) As Integer
        ' match is a "WHERE" query string part (without the "WHERE")

        ' fields is a 2 dimensional array (data field => new value)

        Dim rt As Integer = 0

        Dim q As String
        Dim r As Integer

        Dim c As MySqlCommand

        q = GetDeleteString(Table, Match)

modAgain:

        System.Threading.Thread.Sleep(0)

        If (_Link Is Nothing) OrElse (_Link.State <> ConnectionState.Open) Then
            Me.ReopenLink()
            System.Threading.Thread.Sleep(0)
        End If

        Try
            c = _Link.CreateCommand()

            c.CommandType = CommandType.Text
            c.CommandText = q

            r = c.ExecuteNonQuery()

        Catch ex As Exception
            If (rt >= _MaxTries) Then
                setError(False, ex.HResult, ex.Message)

                If CloseAfterQuery Then CloseLink()
                Return 0
            End If

            _Link = New MySqlConnection()

            rt += 1
            System.Threading.Thread.Sleep(0)

            GoTo modAgain

        End Try

        If CloseAfterQuery Then CloseLink()

        If (r >= 1) Then
            Return r
        End If

        Return 0

    End Function

#Region "Query String Functions"


    ''' <summary>
    ''' Gets a single delete string based upon any kind of predefined match..
    ''' </summary>
    ''' <param name="table">The table to delete from.</param>
    ''' <param name="match">The match parameters.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDeleteString(table As String, match As String) As String
        Return String.Format("DELETE FROM {0} WHERE ({1});", table, match) & vbCrLf
    End Function

    ''' <summary>
    ''' Gets a modify or replace query string based on the specified criteria.
    ''' </summary>
    ''' <param name="table">The table to work with.</param>
    ''' <param name="match">The condition that must exist.</param>
    ''' <param name="data">The data to work with.</param>
    ''' <param name="afterWhere">Any additional text that goes after the WHERE clause.</param>
    ''' <returns>A string suitable for executing in MySql.</returns>
    ''' <remarks></remarks>
    Public Function GetModifyString(table As String, match As String, data() As XRecord, Optional afterWhere As String = Nothing) As String
        Dim t As Integer
        Dim sb As New StringBuilder

        sb.Append(String.Format("UPDATE {0} SET ", table))
        t = 0

        For t = 0 To data.Length - 1

            If (t <> 0) Then sb.Append(", ")

            If (data(t).Value Is Nothing) Then
                sb.Append(String.Format("{0}=NULL", data(t).Key))
            Else
                sb.Append(String.Format("{0}='{1}'", data(t).Key, EscapeString(data(t).Value.ToString)))
            End If

        Next

        sb.Append(" WHERE (" + match + ")")

        If afterWhere IsNot Nothing Then
            sb.Append(" " & afterWhere)
        End If

        sb.Append(";")
        sb.Append(vbCrLf)

        Return sb.ToString
    End Function


    ''' <summary>
    ''' Gets a WHERE clause based on a specific primary key.
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetKeyString(ByVal Key As MySqlRecord) As String
        Dim sb As New StringBuilder
        Dim p As Boolean = False

        sb.Append("(")

        For Each m In Key
            If p Then sb.Append(" AND ") Else p = True
            sb.Append(String.Format("({0}='{1}')", m.Key, EscapeString(m.Value)))
        Next

        sb.Append(")")

        Return sb.ToString

    End Function



    ''' <summary>
    ''' Gets an insert query string based on the specified criteria.
    ''' </summary>
    ''' <param name="table">The table to work with.</param>
    ''' <param name="data">The data to work with.</param>
    ''' <param name="replace">Set to True to replace existing records.</param>
    ''' <returns>A string suitable for executing in MySql.</returns>
    ''' <remarks></remarks>
    Public Function GetInsertString(table As String, data() As XRecord, Optional replace As Boolean = False) As String

        Dim rt As Integer = 0

        Dim i As Integer,
            d As Integer = 0

        Dim sb As New StringBuilder

        System.Threading.Thread.Sleep(0)

        If (replace = True) Then
            sb.Append("INSERT IGNORE ")

        Else
            sb.Append("INSERT ")
        End If

        sb.Append(String.Format("INTO {0}.{1} (", Database, table))

        For i = 0 To data.Length - 1

            If (data(i).Value Is Nothing) Then
                data(i).Value = ""
            End If

            If (Not TypeOf data(i).Value Is String) OrElse (data(i).Value <> "") Then
                If (d <> 0) Then sb.Append(", ")
                sb.Append(data(i).Key)
                d += 1
            End If

        Next

        sb.Append(") VALUES(")
        d = 0

        For i = 0 To data.Length - 1

            If (Not TypeOf data(i).Value Is String) OrElse (data(i).Value <> "") Then
                If (d <> 0) Then sb.Append(", ")
                sb.Append(String.Format("'{0}'", EscapeString(data(i).Value)))
                d += 1
            End If

        Next

        sb.Append(");")
        sb.Append(vbCrLf)

        Return sb.ToString

    End Function

#End Region

#End Region



#Region "New and Finalize"

    Protected Overrides Sub Finalize()
        Me.CloseLink()
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Instantiates a new LinkInterface object
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

    End Sub

    ''' <summary>
    ''' Instantiates a new LinkInterface object
    ''' </summary>
    ''' <param name="defaultCloseLink">True to default to disconnect after every query.</param>
    ''' <remarks></remarks>
    Public Sub New(defaultCloseLink As Boolean)
        CloseAfterQuery = defaultCloseLink
    End Sub

#End Region

End Class
