﻿'' Better MySql result classes.

'' Organizes data from a MySql result much more
'' quickly than other methods.

Option Explicit On

Imports MySql.Data.MySqlClient
Imports System.Runtime.InteropServices

''' <summary>
''' Lightweight raw Key/Value record; provides the basis for this record system.
''' </summary>
''' <remarks></remarks>
<StructLayout(LayoutKind.Sequential, CharSet:=Runtime.InteropServices.CharSet.Unicode)>
Public Structure XRecord
    Public Key As String
    Public Value As Object

    Public Shared Widening Operator CType(ByVal vn As MySqlKeyValuePair) As XRecord
        Return New XRecord With {.Key = vn.Key, .Value = vn.Value}
    End Operator

    Public Shared Widening Operator CType(ByVal s() As String) As XRecord
        Return New XRecord With {.Key = s(0), .Value = s(1)}
    End Operator

    Public Overrides Function ToString() As String

        If Value Is Nothing Then
            Return "{""" & Key & """ : null}"
        Else
            Return "{""" & Key & """ : """ & Value.ToString & """}"
        End If

    End Function

    ''' <summary>
    ''' Instantiate a new structure
    ''' </summary>
    ''' <param name="k">Key</param>
    ''' <param name="v">Value</param>
    ''' <remarks></remarks>
    Public Sub New(k As String, v As Object)
        Key = k
        Value = v
    End Sub

End Structure


''' <summary>
''' Represents a Key/Value pair for a MySql result.
''' </summary>
''' <remarks></remarks>
Public Class MySqlKeyValuePair
    Implements ICloneable

    Private xrec As XRecord

    Public Shared Widening Operator CType(ByVal vn As XRecord) As MySqlKeyValuePair
        Return New MySqlKeyValuePair(vn)
    End Operator

    ''' <summary>
    ''' Clone this Key/Value pair
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return New MySqlKeyValuePair(xrec.Key, xrec.Value)
    End Function

    Public Sub New()

    End Sub

    ''' <summary>
    ''' Initialize a new record from an XRecord
    ''' </summary>
    ''' <param name="xrec"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal xrec As XRecord)
        Me.xrec = xrec
    End Sub

    ''' <summary>
    ''' Initialize a new record from a key and a value
    ''' </summary>
    ''' <param name="Key">Key or Column Name</param>
    ''' <param name="Value">Value</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal Key As String, ByVal Value As Object)
        xrec.Key = Key
        xrec.Value = Value
    End Sub

    ''' <summary>
    ''' Gets or sets the Key or Column name of this object
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Key() As String
        Get
            Return xrec.Key
        End Get
        Set(ByVal value As String)
            xrec.Key = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the Value of this object
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Value() As Object
        Get
            Return xrec.Value
        End Get
        Set(ByVal value As Object)
            xrec.Value = value
        End Set
    End Property

    ''' <summary>
    ''' Returns this object as a string representation.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function ToString() As String
        Return Key & " : " & Value
    End Function

    ''' <summary>
    ''' Returns this object as a string representation.
    ''' </summary>
    ''' <param name="asJson">True to return a JSON-style value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function ToString(asJson As Boolean) As String
        If asJson Then Return """" & Key & """:""" & Value & """" Else Return ToString()
    End Function

End Class

''' <summary>
''' Represents a MySql Record
''' </summary>
''' <remarks></remarks>
Public Class MySqlRecord
    Inherits List(Of MySqlKeyValuePair)

    ''' <summary>
    ''' Clone this object.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Clone() As MySqlRecord
        Dim n As New MySqlRecord

        For Each h In Me
            n.Add(New MySqlKeyValuePair(h))
        Next

        Return (n)
    End Function

    ''' <summary>
    ''' Returns true if this record contains a column.
    ''' </summary>
    ''' <param name="ColumnName">Column name to look for.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HasColumn(ByVal ColumnName As String) As Boolean
        Dim s() As String = Me.Columns
        Dim i As Integer

        For i = 0 To s.Length - 1
            If (s(i) = ColumnName) Then Return True
        Next

        Return False
    End Function

    ''' <summary>
    ''' Returns all columns as a string array.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Columns() As String()
        Get

            If Me.Count = 0 Then Return Nothing
            Dim c As New List(Of String)

            For Each kvp As MySqlKeyValuePair In Me
                c.Add(kvp.Key)
            Next

            Return c.ToArray
        End Get
    End Property

    ''' <summary>
    ''' Returns the name of the column at the given 0-based index.
    ''' </summary>
    ''' <param name="Index"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ColumnName(ByVal Index As Integer) As String
        Get
            Return Me(Index).Key
        End Get
    End Property

    ''' <summary>
    ''' Add a column with a value.
    ''' </summary>
    ''' <param name="Key">Key or column name.</param>
    ''' <param name="Value">Value.</param>
    ''' <remarks></remarks>
    Public Overloads Sub Add(ByVal Key As String, ByVal Value As Object)
        ' Add or Update a Key
        On Error Resume Next

        Dim b As MySqlKeyValuePair = Nothing

        b = Me.Item(Key)
        If Not b Is Nothing Then
            Item(Key).Value = Value
            Exit Sub
        End If

        b = New MySqlKeyValuePair(Key, Value)
        Me.Add(b)
    End Sub

    ''' <summary>
    ''' Remove by Key.
    ''' </summary>
    ''' <param name="Key">Column or key to remove.</param>
    ''' <remarks></remarks>
    Public Overloads Sub Remove(ByVal Key As String)
        Try

            Dim i As Integer,
                c As Integer = Me.Count - 1

            For i = 0 To c
                If Me(i).Key = Key Then
                    Me.RemoveAt(i)
                    Return
                End If
            Next
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    ''' <summary>
    ''' Gets or sets the Value of the item as the specified Key
    ''' </summary>
    ''' <param name="Key">Key or column name</param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Default Public Overloads Property Item(ByVal Key As String) As Object
        Get
            Dim i As Integer,
                c As Integer = Me.Count - 1

            For i = 0 To c
                If Me(i).Key = Key Then
                    Return Me(i).Value
                End If
            Next
            Return Nothing
        End Get
        Set(ByVal value As Object)
            Dim i As Integer,
                c As Integer = Me.Count - 1

            For i = 0 To c
                If Me(i).Key = Key Then
                    Me(i).Value = value
                    Return
                End If
            Next
        End Set
    End Property

    ''' <summary>
    ''' Get the MySqlKeyValuePair object for the specified Key.
    ''' </summary>
    ''' <param name="Key">Key or column name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetKeyValuePair(ByVal Key As String) As MySqlKeyValuePair
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).Key = Key Then
                Return Me(i)
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' Gets the MySqlKeyValuePair object by index.
    ''' </summary>
    ''' <param name="Index"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetKeyValuePair(ByVal Index As Integer) As MySqlKeyValuePair
        Return Me(Index)
    End Function

    Public Function GetKeyValuePairByValue(ByVal Value As String) As MySqlKeyValuePair
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).Value = Value Then
                Return Me(i)
            End If
        Next
        Return Nothing
    End Function

    Public Sub New()
        '
    End Sub

    ''' <summary>
    ''' Sets the MySqlRecord from the XRecord
    ''' </summary>
    ''' <param name="xrec"></param>
    ''' <remarks></remarks>
    Public Sub SetXRecord(ByVal xrec() As XRecord)

        Dim i As Integer,
            j As Integer

        i = xrec.Length
        For j = 0 To i - 1
            Me.Add(New MySqlKeyValuePair(xrec(j)))
        Next

    End Sub

    ''' <summary>
    ''' Returns true if this record contains this column.
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ContainsKey(key As String) As Boolean
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).Key = key Then
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Returns true if this record contains this value in any column.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ContainsValue(value As String) As Boolean
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).Value.Equals(value) Then
                Return True
            End If
        Next

        Return False
    End Function


    Public Overloads Function Contains(key As String, value As String) As Boolean
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).Value.Equals(value) AndAlso Me(i).Key = key Then
                Return True
            End If
        Next

        Return False

    End Function

    ''' <summary>
    ''' Build an XRecord of this record
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetXRecord() As XRecord()
        Dim o() As XRecord,
            m As MySqlKeyValuePair,
            i As Integer = 0

        If (Me.Count = 0) Then Return Nothing

        ReDim o(Me.Count - 1)

        For Each m In Me
            o(i).Key = m.Key
            o(i).Value = m.Value
            i += 1
        Next

        Return o

    End Function

    ''' <summary>
    ''' Eliminates duplicate columns.
    ''' </summary>
    ''' <returns>Number of columns deleted.</returns>
    ''' <remarks></remarks>
    Public Function EliminateDuplicateColumns() As Integer

        Dim seen As New List(Of String)
        Dim take As New List(Of Integer)
        Dim i As Integer

        i = 0
        For Each z In Me


            If seen.Contains(z.Key) Then
                take.Add(i)
            Else
                seen.Add(z.Key)
            End If
            i += 1
        Next

        take.Reverse()

        For Each i In take
            Me.RemoveAt(i)
        Next

        Return take.Count
    End Function

    ''' <summary>
    ''' Initialize a new object with the XRecord array.
    ''' </summary>
    ''' <param name="xrec"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal xrec() As XRecord)
        Me.SetXRecord(xrec)
    End Sub

    Public Shared Widening Operator CType(operand As MySqlRecord) As XRecord()
        Return operand.GetXRecord
    End Operator

    Public Shared Widening Operator CType(operand() As XRecord) As MySqlRecord
        Return New MySqlRecord(operand)
    End Operator

    Public Shared Operator =(value1 As MySqlRecord, value2 As MySqlRecord) As Boolean
        If value1.Count <> value2.Count Then Return False

        For i = 0 To value1.Count - 1

            If value1(i).Key <> value2(i).Key Then Return False
            If Not value1(i).Value.Equals(value2(i).Value) Then Return False

        Next

        Return True
    End Operator


    Public Shared Operator <>(value1 As MySqlRecord, value2 As MySqlRecord) As Boolean
        Return Not (value1 = value2)
    End Operator

    '' Supports concatenation!

    Public Shared Operator &(value1 As MySqlRecord, value2 As MySqlRecord) As MySqlRecord
        value1.AddRange(value2.ToArray)
        value1.EliminateDuplicateColumns()
        Return value1
    End Operator

    Public Shared Operator +(value1 As MySqlRecord, value2 As MySqlRecord) As MySqlRecord
        Return value1 & value2
    End Operator

End Class

''' <summary>
''' Represents a collection of MySqlRecords, or an entire Query result.
''' </summary>
''' <remarks></remarks>
Public Class MySqlResult
    Inherits List(Of MySqlRecord)

    ''' <summary>
    ''' Returns the Field or Column name at the specified index.
    ''' </summary>
    ''' <param name="Index"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Field(ByVal Index As Integer) As String
        Get
            Return Me(0).ColumnName(Index)
        End Get
    End Property

    ''' <summary>
    ''' Returns a string array containing a list of all field or column names.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllFields() As String()

        If Me.Count = 0 Then Return Nothing
        Return Me.Item(0).Columns

    End Function

    ''' <summary>
    ''' Search for a record containing the specified key/value pair.
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <param name="Value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRecordByKeyValuePair(ByVal Key As String, ByVal Value As String) As MySqlRecord
        Dim i As Integer,
            c As Integer = Me.Count - 1

        For i = 0 To c
            If Me(i).ContainsKey(Key) AndAlso Me(i).Item(Key) = Value Then
                Return Me(i)
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' Create a new MySqlResult based on a 2-dimensional array of XRecords
    ''' </summary>
    ''' <param name="xrec"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal xrec(,) As XRecord)

        Dim i As Integer,
            j As Integer

        i = xrec.GetLength(0)
        For j = 0 To i - 1
            Me.Add(New MySqlRecord(ExtractOneKVRecord(j, xrec)))
        Next

    End Sub



    ''' <summary>
    ''' Determines whether this record contains the specified key with the specified value.
    ''' </summary>
    ''' <param name="key">Key to compare</param>
    ''' <param name="value">Value to compare</param>
    ''' <returns>True if this record is exists.</returns>
    ''' <remarks>Object.Equals() is used to compare values</remarks>
    Public Overloads Function Contains(key As String, value As Object) As Boolean

        For Each m In Me

            For Each n In m
                If n.Key = key Then
                    Return n.Value.Equals(value)
                End If
            Next
        Next

        Return False
    End Function

    Public Sub New()

    End Sub

    Public Sub New(result1 As MySqlResult, result2 As MySqlResult)

        AddRange(result1.ToArray)
        AddRange(result2.ToArray)

    End Sub

    '' Concatenation is supported!

    Public Shared Operator &(value1 As MySqlResult, value2 As MySqlResult) As MySqlResult
        value1.AddRange(value2.ToArray)
        Return value1
    End Operator

    Public Shared Operator +(value1 As MySqlResult, value2 As MySqlResult) As MySqlResult
        value1.AddRange(value2.ToArray)
        Return value1
    End Operator

    Public Shared Operator =(value1 As MySqlResult, value2 As MySqlResult) As Boolean
        If value1.Count <> value2.Count Then Return False

        For i = 0 To value1.Count - 1
            If value1(i) <> value2(i) Then Return False
        Next

        Return True
    End Operator

    Public Shared Operator <>(value1 As MySqlResult, value2 As MySqlResult) As Boolean
        Return Not (value1 = value2)
    End Operator


    ''' <summary>
    ''' Extracts a single XRecord from a 2-dimensional array.
    ''' </summary>
    ''' <param name="Index">Index of the record to extract</param>
    ''' <param name="xrec">Haystack of XRecords to search</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExtractOneKVRecord(ByVal Index As Integer, ByVal xrec(,) As XRecord) As XRecord()

        Dim i As Integer,
            j As Integer,
            x As Integer

        Dim s() As XRecord

        i = xrec.GetLength(0)
        x = xrec.GetLength(1)

        If Index >= i Then Return Nothing
        If Index < 0 Then Return Nothing

        ReDim s(x - 1)
        For j = 0 To x - 1
            s(j) = xrec(Index, j)
        Next

        Return s

    End Function

End Class