﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLMBrowser
{
    public class Config
    {
        public List<string> ZipFiles { get; set; } = new List<string>();
        public string DataPath { get; set; }

        public Config()
        {
            DataPath = Cache.CacheDirectory;
        }

        public Config(string path, List<string> files) 
        {
            ZipFiles = files;
            DataPath = path;
        }

    }
}
