﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NLMBrowser
{
    public class ImageInfo
    {

        public string Name { get; set; }

        public BitmapSource Image { get; set; }

    }
}
