﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Environment;
using System.IO;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace NLMBrowser
{
    public static class Cache
    {
        public static string CacheDirectory { get; private set; }

        public static void Init()
        {
            CacheDirectory = GetFolderPath(SpecialFolder.ApplicationData) + "\\com.precise-soft.NLMBrowser.Cache";

            if (!Directory.Exists(CacheDirectory))
                Directory.CreateDirectory(CacheDirectory);

        }

        public static string LastPath
        {
            get
            {
                if (!App.Current.Properties.Contains("LastPath"))
                    App.Current.Properties.Add("LastPath", CacheDirectory);

                return (string)App.Current.Properties["LastPath"];
            }
            set
            {
                if (App.Current.Properties.Contains("LastPath"))
                    App.Current.Properties.Remove("LastPath");

                App.Current.Properties.Add("LastPath", value);
            }
        }

        public static void ClearIndexFile()
        {
            string fpth = $"{CacheDirectory}\\entries.json";
            if (File.Exists(fpth)) File.Delete(fpth);
        }

        public static void WriteIndexFile (List<IndexEntry> entries)
        {
            ClearIndexFile();

            string fpth = $"{CacheDirectory}\\entries.json";

            string c = JsonConvert.SerializeObject(entries);
            byte[] b = Encoding.UTF8.GetBytes(c);

            FileStream fo = new FileStream(fpth, FileMode.CreateNew);

            fo.Write(b, 0, b.Length);
            fo.Close();
        }

        public static List<IndexEntry> ReadIndexFile()
        {
            string fpth = $"{CacheDirectory}\\entries.json";
            byte[] b;
            string c;

            if (!File.Exists(fpth)) return new List<IndexEntry>();
            
            FileStream fo = new FileStream(fpth, FileMode.Open);
            b = new byte[fo.Length];

            fo.Read(b, 0, b.Length);
            fo.Close();

            c = Encoding.UTF8.GetString(b);

            return JsonConvert.DeserializeObject<List<IndexEntry>>(c);
        }

        public static void ClearRxNormFile()
        {
            string fpth = $"{CacheDirectory}\\rxnorm.json";
            if (File.Exists(fpth)) File.Delete(fpth);
        }

        public static void WriteRxNormFile(List<RxNormMapping> rxnorm)
        {
            ClearRxNormFile();

            string fpth = $"{CacheDirectory}\\rxnorm.json";

            string c = JsonConvert.SerializeObject(rxnorm);
            byte[] b = Encoding.UTF8.GetBytes(c);

            FileStream fo = new FileStream(fpth, FileMode.CreateNew);

            fo.Write(b, 0, b.Length);
            fo.Close();
        }

        public static List<RxNormMapping> ReadRxNormFile()
        {
            string fpth = $"{CacheDirectory}\\rxnorm.json";
            byte[] b;
            string c;

            if (!File.Exists(fpth)) return new List<RxNormMapping>();

            FileStream fo = new FileStream(fpth, FileMode.Open);
            b = new byte[fo.Length];

            fo.Read(b, 0, b.Length);
            fo.Close();

            c = Encoding.UTF8.GetString(b);

            return JsonConvert.DeserializeObject<List<RxNormMapping>>(c);
        }


        public static void ClearConfigFile()
        {
            string fpth = $"{CacheDirectory}\\config.json";
            if (File.Exists(fpth)) File.Delete(fpth);
        }

        public static void WriteConfigFile(Config config)
        {
            ClearConfigFile();

            string fpth = $"{CacheDirectory}\\config.json";

            string c = JsonConvert.SerializeObject(config);
            byte[] b = Encoding.UTF8.GetBytes(c);

            FileStream fo = new FileStream(fpth, FileMode.CreateNew);

            fo.Write(b, 0, b.Length);
            fo.Close();
        }

        public static Config ReadConfig()
        {
            string fpth = $"{CacheDirectory}\\config.json";
            byte[] b;
            string c;

            if (!File.Exists(fpth)) return new Config();

            FileStream fo = new FileStream(fpth, FileMode.Open);
            b = new byte[fo.Length];

            fo.Read(b, 0, b.Length);
            fo.Close();

            c = Encoding.UTF8.GetString(b);

            return JsonConvert.DeserializeObject<Config>(c);
        }

    }
}
