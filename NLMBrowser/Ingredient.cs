﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLMBrowser
{
    public enum IngredientType
    {
        Active,
        Inactive
    }

    public class Ingredient
    {
        public IngredientType Type { get; set; } = IngredientType.Active;

        public int RxCui { get; set; }

        public string Name { get; set; }

        public string NumeratorType { get; set; }

        public float NumeratorAmount { get; set; }

        public string DenominatorType { get; set; }

        public float DenominatorAmount { get; set; }

        public override string ToString()
        {

            string dstr = "";

            if (DenominatorType == "each")
                dstr = $" {NumeratorAmount} {NumeratorType}";
            else if (DenominatorAmount == 1)
                dstr = $" {NumeratorAmount} {NumeratorType}/{DenominatorType}";
            else
                dstr = $" {NumeratorAmount} {NumeratorType}/{DenominatorAmount} {DenominatorType}";

            return Name + dstr;
        }

    }
}
