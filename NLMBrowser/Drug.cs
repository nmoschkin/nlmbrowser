﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DataTools.Strings;
using SqlLib;

namespace NLMBrowser
{
    public class Drug
    {

        public string Manufacturer { get; set; }

        private string name;

        public string Name
        {
            get => name;
            set => name = TextTools.CamelCase(value);
        }

        public Guid Guid { get; set; }

        public List<Dosage> Dosages { get; set; } = new List<Dosage>();

        public string Description { get; set; }

        public string BlackBoxWarning { get; set; }

        protected Drug()
        {
            
        }

        public List<MySqlRecord> GetIngredients(int dosageIdx, int dosageId)
        {
            MySqlRecord m;
            var l = new List<MySqlRecord>();

            var dose = Dosages[dosageIdx];

            foreach (var d in dose.ActiveIngredients)
            {
                m = new MySqlRecord();

                m.Add(new MySqlKeyValuePair("drugid", dosageId.ToString()));
                m.Add(new MySqlKeyValuePair("name", TextTools.TitleCase(d.Name)));
                m.Add(new MySqlKeyValuePair("active", "1"));

                m.Add(new MySqlKeyValuePair("dosage_numerator_value", d.NumeratorAmount));
                m.Add(new MySqlKeyValuePair("dosage_numerator_unit", d.NumeratorType));
                m.Add(new MySqlKeyValuePair("dosage_denominator_value", d.DenominatorAmount));
                m.Add(new MySqlKeyValuePair("dosage_denominator_unit", d.DenominatorType));

                l.Add(m);
            }

            foreach (var d in dose.InactiveIngredients)
            {
                m = new MySqlRecord();

                m.Add(new MySqlKeyValuePair("drugid", dosageId.ToString()));
                m.Add(new MySqlKeyValuePair("name", TextTools.TitleCase(d.Name)));
                m.Add(new MySqlKeyValuePair("active", "0"));

                m.Add(new MySqlKeyValuePair("dosage_numerator_value", d.NumeratorAmount));
                m.Add(new MySqlKeyValuePair("dosage_numerator_unit", d.NumeratorType));
                m.Add(new MySqlKeyValuePair("dosage_denominator_value", d.DenominatorAmount));
                m.Add(new MySqlKeyValuePair("dosage_denominator_unit", d.DenominatorType));

                l.Add(m);
            }


            return l;

        }

        public List<MySqlRecord> GetDosages()
        {
            MySqlRecord m;
            var l = new List<MySqlRecord>();


            foreach (var d in Dosages)
            {
                m = new MySqlRecord();

                m.Add(new MySqlKeyValuePair("guid", Guid.ToString("D")));
                m.Add(new MySqlKeyValuePair("nlmcode", d.NLMCode));

                m.Add(new MySqlKeyValuePair("name", TextTools.TitleCase(name)));

                m.Add(new MySqlKeyValuePair("generic_name", TextTools.TitleCase(d.GenericName)));

                if (!string.IsNullOrEmpty(Manufacturer))
                {
                    m.Add(new MySqlKeyValuePair("manufacturer", Manufacturer));
                }

                m.Add(new MySqlKeyValuePair("size_unit", d.SizeUnit));
                m.Add(new MySqlKeyValuePair("size_value", d.SizeValue.ToString()));


                if (!string.IsNullOrEmpty(d.RoutesList))
                {
                    m.Add(new MySqlKeyValuePair("route", d.RoutesList));
                }

                if (!string.IsNullOrEmpty(d.Imprint))
                {
                    m.Add(new MySqlKeyValuePair("imprint", d.Imprint));
                }

                m.Add(new MySqlKeyValuePair("score", d.Score.ToString()));

                if (!string.IsNullOrEmpty(d.Color))
                {
                    m.Add(new MySqlKeyValuePair("color", d.Color.ToUpper()));
                }

                if (!string.IsNullOrEmpty(d.Shape))
                {
                    m.Add(new MySqlKeyValuePair("shape", d.Shape.ToUpper()));
                }

                l.Add(m);
            }

            return l;
        }

        public static Drug FromXml(string xml, Guid id)
        {
            Drug outDrug = new Drug();
            XmlNodeList a;
            List<XmlNode> products = new List<XmlNode>();

            Dosage dose;
            Ingredient ing;
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            a = doc.GetElementsByTagName("name");

            outDrug.Guid = id;

            foreach (XmlNode node in a)
            {
                if (node.ParentNode.Name == "representedOrganization")
                {
                    outDrug.Manufacturer = node.InnerText;
                    break;
                }
            }

            a = doc.GetElementsByTagName("manufacturedProduct");

            foreach (XmlNode node in a) 
            {
                if (node.ParentNode.Name == "manufacturedProduct")
                {
                    products.Add(node);
                }
            }

            foreach (XmlNode node in products)
            {
                // New Dosage, let's consume it.

                dose = new Dosage();

                foreach (XmlNode member in node.ChildNodes)
                {

                    if (member.Name == "code")
                    {
                        dose.NLMCode = TextTools.TitleCase(member.Attributes["code"].Value);
                    }
                    else if (member.Name == "name")
                    {
                        dose.Name = TextTools.TitleCase(member.InnerText);
                    }
                    else if (member.Name == "formCode")
                    {
                        dose.Description = member.Attributes["displayName"].Value;
                    }
                    else if (member.Name == "asEntityWithGeneric")
                    {
                        foreach (XmlNode xGen in member.ChildNodes)
                        {
                            if (xGen.Name == "genericMedicine")
                            {
                                foreach (XmlNode xGen2 in xGen.ChildNodes)
                                {
                                    if (xGen2.Name == "name")
                                    {
                                        dose.GenericName = TextTools.TitleCase(xGen2.InnerText);
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    else if (member.Name == "ingredient")
                    {
                        if (member.Attributes["classCode"].Value.Substring(0, 4) == "ACTI")
                        {
                            ing = new Ingredient
                            {
                                Type = IngredientType.Active,
                                Name = TextTools.TitleCase(member.ChildNodes[1].ChildNodes[1].InnerText),

                                NumeratorType = member.ChildNodes[0].ChildNodes[0].Attributes["unit"].Value,
                                NumeratorAmount = float.Parse(member.ChildNodes[0].ChildNodes[0].Attributes["value"].Value),

                                DenominatorType = member.ChildNodes[0].ChildNodes[1].Attributes["unit"].Value,
                                DenominatorAmount = float.Parse(member.ChildNodes[0].ChildNodes[1].Attributes["value"].Value)
                            };

                            if (ing.DenominatorType == "1") ing.DenominatorType = "each";

                            dose.ActiveIngredients.Add(ing);

                        }
                        else if (member.Attributes["classCode"].Value == "IACT")
                        {
                            ing = new Ingredient
                            {
                                Type = IngredientType.Inactive,
                                Name = TextTools.TitleCase(member.ChildNodes[0].ChildNodes[1].InnerText)
                            };
                            dose.InactiveIngredients.Add(ing);
                        }
                    }

                    foreach (XmlNode subj in node.ParentNode.ChildNodes)
                    {
                        if (subj.Name == "subjectOf")
                        {
                            try { 
                                if (subj.ChildNodes[0].Name == "characteristic" /*&& subj.ChildNodes[0].Attributes["classCode"].Value == "OBS"*/)
                                {
                                    if (subj.ChildNodes[0].ChildNodes.Count < 2) continue;

                                    var xType = subj.ChildNodes[0].ChildNodes[0];
                                    var xDesc = subj.ChildNodes[0].ChildNodes[1];

                                    switch (xType.Attributes["code"].Value)
                                    {

                                        case "SPLCOLOR":
                                            dose.Color = xDesc.Attributes["displayName"].Value;
                                            break;

                                        case "SPLSHAPE":
                                            dose.Shape = xDesc.Attributes["displayName"].Value;
                                            break;

                                        case "SPLIMPRINT":
                                            dose.Imprint = xDesc.InnerText;
                                            break;

                                        case "SPLSCORE":
                                            dose.Score = int.Parse(xDesc.Attributes["value"].Value);
                                            break;

                                        case "SPLSIZE":
                                            dose.SizeUnit = xDesc.Attributes["unit"].Value;
                                            dose.SizeValue = float.Parse(xDesc.Attributes["value"].Value);
                                            break;

                                    }

                                }

                            }
                            catch { }

                        }
                        else if (subj.Name == "consumedIn")
                        {
                            if (subj.ChildNodes[0].Name == "substanceAdministration")
                            {
                                if (subj.ChildNodes[0].ChildNodes[0].Name == "routeCode")
                                {
                                    if (subj.ChildNodes[0].ChildNodes[0].Attributes["displayName"] != null)
                                    {
                                        var route = subj.ChildNodes[0].ChildNodes[0].Attributes["displayName"].Value;
                                        if (!string.IsNullOrEmpty(route)) 
                                            route = route.ToUpper();

                                        if (!dose.Routes.Contains(route))
                                        {
                                            dose.Routes.Add(route);
                                        }

                                    }

                                }
                            }
                        }

                    }

                }

                outDrug.Dosages.Add(dose);
            }

            a = doc.GetElementsByTagName("code");

            foreach (XmlNode node in a)
            {
                if (node.ParentNode.Name ==  "section" && node.Attributes["displayName"]?.Value.ToUpper() == "DESCRIPTION SECTION")
                {
                    foreach (XmlNode p in node.ParentNode.ChildNodes)
                    {
                        if (p.Name == "text" || p.Name == "excerpt")
                        {
                            outDrug.Description = p.InnerText;
                            break;
                        }
                    }

                    break;
                }
            }

            foreach (XmlNode node in a)
            {
                if (node.ParentNode.Name == "section" && node.Attributes["displayName"]?.Value.ToUpper() == "OTC - PURPOSE SECTION")
                {
                    foreach (XmlNode p in node.ParentNode.ChildNodes)
                    {
                        if (p.Name == "text"  || p.Name == "excerpt")
                        {
                            if (!string.IsNullOrEmpty(outDrug.Description)) outDrug.Description += "\r\n\r\n";
                            outDrug.Description += p.InnerText;
                            break;
                        }
                    }

                    break;
                }
            }


            foreach (XmlNode node in a)
            {
                var ss = node.Attributes["displayName"]?.Value.ToUpper() ?? "";
                if (node.ParentNode.Name == "section" && ss.Contains("USAGE SECTION"))
                {
                    foreach (XmlNode p in node.ParentNode.ChildNodes)
                    {
                        if (p.Name == "text" || p.Name == "excerpt")
                        {
                            if (!string.IsNullOrEmpty(outDrug.Description)) outDrug.Description += "\r\n\r\n";
                            outDrug.Description += p.InnerText;
                            break;
                        }
                    }

                    break;
                }
            }

            foreach (XmlNode node in a)
            {
                var ss = node.Attributes["displayName"]?.Value.ToUpper() ?? "";
                if (node.ParentNode.Name == "section" && ss.Contains("WARNING"))
                {
                    foreach (XmlNode p in node.ParentNode.ChildNodes)
                    {
                        if (p.Name == "text" || p.Name == "excerpt")
                        {
                            if (!string.IsNullOrEmpty(outDrug.BlackBoxWarning)) outDrug.BlackBoxWarning += "\r\n\r\n";
                            outDrug.BlackBoxWarning = p.InnerText;
                            break;
                        }
                    }

                    break;
                }
            }

            if (outDrug.Dosages.Count > 0)
            {
                outDrug.Name = outDrug.Dosages[0].Name;
            }
            
            return outDrug; 
        }


    }
}
