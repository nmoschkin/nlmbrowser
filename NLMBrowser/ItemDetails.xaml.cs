﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace NLMBrowser
{

    /// <summary>
    /// Interaction logic for ItemDetails.xaml
    /// </summary>
    public partial class ItemDetails : Window
    {

        private IndexEntry entry;

        Drug drug;

        public IndexEntry Entry
        {
            get => entry;
            set
            {
                if (entry == value) return;
                entry = value;
            }
        }

        public ItemDetails(IndexEntry entry)
        {
            Entry = entry;
            InitializeComponent();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (Entry == null)
            {
                this.Close();
                return;
            }

            Entry.Images.Clear();
            Entry.Read();

            Images.ItemsSource = Entry.Images;
            //XMLArea.Text = Entry.Xml;

            drug = Drug.FromXml(Entry.Xml, Entry.Id);

            this.DataContext = drug;

            Clipboard.SetText(Entry.Xml);
        }

        private void Images_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var img = Images.SelectedItem as ImageInfo;
            if (img == null) return;


            var winImg = new ImageShow();
            winImg.Title = img.Name;
            winImg.ImageToShow.Source = img.Image;

            winImg.Show();
        }
    }
}
