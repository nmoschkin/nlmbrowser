﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLMBrowser
{
    public class Dosage
    {
        public string NLMCode { get; set; }

        public string Name { get; set; }

        public string GenericName { get; set; }

        public string Description { get; set; }

        public List<Ingredient> ActiveIngredients { get; set; } = new List<Ingredient>();

        public List<Ingredient> InactiveIngredients { get; set; } = new List<Ingredient>();

        public DateTime Timestamp { get; set; }

        public string Color { get; set; }

        public string Shape { get; set; }

        public string Imprint { get; set; }

        public string SizeUnit { get; set; }

        public float SizeValue { get; set; }

        public int Score { get; set; }

        public List<string> Routes { get; set; } = new List<string>();

        public string RoutesList
        {
            get
            {
                string s = "";

                foreach (var r in Routes)
                {
                    if (s != "") s += ",";
                    s += r;
                }

                return s;
            }
        }

        public override string ToString()
        {
            if (ActiveIngredients.Count > 0)
            {
                string s = "";
                foreach (var act in ActiveIngredients)
                {
                    if (s != "") s += ", ";
                    s += act.ToString();
                }

                return s;
            }

            else return Name;
        }

    }
}
