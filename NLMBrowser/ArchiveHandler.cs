﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NLMBrowser
{

    public class ArchiveHandler : INotifyPropertyChanged
    {

        public delegate void ReadingFileEvent(object sender, ReadingFileEventArgs e);
        public event ReadingFileEvent ReadingFile;

        public int ReadingFileEventFireInterval { get; set; } = 1024;

        public const string RX_NORM = "rxnorm_mappings.zip";

        public const string RX_PREFIX = "dm_spl_release_human_rx_part";
        public const string OTC_PREFIX = "dm_spl_release_human_otc_part";

        private List<RxNormMapping> rx = new List<RxNormMapping>();

        public List<RxNormMapping> RxIndices
        {
            get => rx;
            set
            {
                if (rx == value) return;
                rx = value;
                OnPropertyChanged();
            }
        }

        public string Path { get; protected set; }

        public ArchiveHandler(string path)
        {
            SetPath(path);
        }

        public ArchiveHandler()
        {

        }

        public void SetPath(string path)
        {
            if (string.IsNullOrEmpty(path) || !System.IO.Directory.Exists(path))
            {
                this.Path = null;
                return;
                // throw new ArgumentException($"Path '{path}' must be non-null and must exist!");
            }

            if (path.LastIndexOf("\\") == path.Length - 1)
            {
                path = path.Substring(0, path.Length - 1);
            }

            this.Path = path;
        }

        public bool ScanDir()
        {
            if (string.IsNullOrEmpty(Path) || !System.IO.Directory.Exists(Path))
            {
                return false;
            }

            List<string> af = new List<string>();

            af.AddRange(Directory.GetFiles(Path, RX_PREFIX + "*.zip"));
            af.AddRange(Directory.GetFiles(Path, OTC_PREFIX + "*.zip"));

            ReadingFileEventArgs ev = null;
            int x = 0;
            App.Current.Dispatcher.Invoke(() =>
            {
                int cc = af.Count;
                int xx = 0;

                ev = new ReadingFileEventArgs("Beginning Index of Zip Files...", "Starting", xx, cc, ReadingFileState.Starting);
                ReadingFile?.Invoke(this, ev);
            });

            foreach (var zipFile in af)
            {

                App.Current.Dispatcher.Invoke(() =>
                {
                    int cc = af.Count;
                    int xx = ++x;

                    ev = new ReadingFileEventArgs($"Indexing {zipFile} ...", "Indexing", xx, cc, ReadingFileState.Reading);
                    ReadingFile?.Invoke(this, ev);

                });

                if (ev?.Cancel == true) break;
                IndexEntry.IndexZipSource(zipFile);
            }

            App.Current.Dispatcher.Invoke(() =>
            {
                int cc = af.Count;
                int xx = 0;

                if (ev.Cancel)
                {
                    ev = new ReadingFileEventArgs($"Indexing Cancelled", "Cancelled", xx, cc, ReadingFileState.Finished);
                }
                else
                {
                    ev = new ReadingFileEventArgs($"Indexing Complete", "Done", xx, cc, ReadingFileState.Finished);
                }
                ReadingFile?.Invoke(this, ev);
            });

            return true;
        }

        public Task<int> ReadRxNormIndices()
        {
            var t = new Task<int>(() =>
            {

                try
                {
                    var z = ZipFile.OpenRead($"{Path}\\{RX_NORM}");
                    var e = z.Entries;
                    var st = e[0].Open();
                    int blen = (int)e[0].Length;
                    byte[] buffer = new byte[blen];
                    st.Read(buffer, 0, blen);

                    string sbuff = Encoding.UTF8.GetString(buffer);
                    st.Close();
                    z.Dispose();

                    sbuff = sbuff.Replace("\r\n", "\n");
                    sbuff = sbuff.Replace("\r", "\n");

                    string[] lines = sbuff.Split('\n');

                    int x = 0;
                    int c = lines.Count();

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        RxIndices = new List<RxNormMapping>();
                    });

                    ReadingFileEventArgs ev = null;

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        int cc = c;
                        int xx = x;

                        ev = new ReadingFileEventArgs("Reading RxNorm Map", "Starting", xx, cc, ReadingFileState.Starting);
                        ReadingFile?.Invoke(this, ev);
                    });

                    RxNormMapping map = null;
                    RxNormMapping pmap = null;

                    foreach (var l in lines)
                    {
                        x++;
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            pmap = RxNormMapping.FromMapping(l);
                            if (pmap?.RxCui.Count == 0) return;

                            if (map != null && map.Uuid == pmap.Uuid)
                            {
                                if (!map.RxCui.Contains(pmap.RxCui[0]))
                                    map.RxCui.Add(pmap.RxCui[0]);
                            }
                            else
                            {
                                map = pmap;
                                if (map.Uuid != Guid.Empty)
                                    RxIndices.Add(map);
                            }

                            if (x % ReadingFileEventFireInterval == 0)
                            {
                                int cc = c;
                                int xx = x;

                                ev = new ReadingFileEventArgs("Reading RxNorm Map", "Processing", xx, cc);
                                ReadingFile?.Invoke(this, ev);

                            }
                        });

                        if (ev.Cancel) break;
                    }

                    if (!ev.Cancel)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {
                            int cc = c;
                            int xx = x;

                            ev = new ReadingFileEventArgs("Reading RxNorm Map", "Starting", xx, cc, ReadingFileState.Finished);
                            ReadingFile?.Invoke(this, ev);
                        });
                    }
                    else
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {
                            int cc = c;
                            int xx = x;

                            ev = new ReadingFileEventArgs("Reading RxNorm Map", "Starting", xx, cc, ReadingFileState.Cancelled);
                            ReadingFile?.Invoke(this, ev);
                        });
                    }

                    GC.Collect(0);
                    return RxIndices.Count();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return 0;
                }

            });

            t.Start();
            return t;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }

    public enum ReadingFileState
    {
        Starting = -1,
        Reading = 0,
        Finished = 1,
        Cancelled = 2

    }

    public class ReadingFileEventArgs : EventArgs
    {
        public string Message { get; internal set; }

        public string Title { get; internal set; }

        public bool Cancel { get; set; }

        public double Progress { get; internal set; } = 0.0;

        public double Count { get; internal set; } = 100.0;

        public ReadingFileState State { get; internal set; }

        public ReadingFileEventArgs(string title, string message, double progress, double count, ReadingFileState state = ReadingFileState.Reading)
        {
            Title = title;
            Message = message;
            Progress = progress;
            Count = count;
            State = state;
        }

        internal ReadingFileEventArgs()
        {

        }

    }

}
