﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace NLMBrowser
{
    public class RxNormMapping : INotifyPropertyChanged
    {

        private Guid uuid;
        private List<int> rxcui = new List<int>();
        private string desc;
        private bool mapped;
        private IndexEntry mappedItem;

        [JsonIgnore()]
        public IndexEntry MappedItem
        {
            get => mappedItem;
            set
            {
                if (mappedItem == value) return;
                mappedItem = value;
                OnPropertyChanged();
            }
        }

        public Guid Uuid
        {
            get => uuid;
            set
            {
                if (uuid == value) return;
                uuid = value;
                OnPropertyChanged();
            }
        }

        public List<int> RxCui
        {
            get => rxcui;
            set
            {
                if (rxcui == value) return;
                rxcui = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => desc;
            set
            {
                if (desc == value) return;
                desc = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore()]
        public bool IsMapped
        {
            get => mapped;
            set
            {
                if (mapped == value) return;
                mapped = value;
                OnPropertyChanged();
            }
        }

        public string RxCuiList
        {
            get
            {
                string s = "";
                foreach (var rx in rxcui)
                {
                    if (s != "") s += ",";
                    s += rx.ToString();
                }

                return s;
            }
        }



        public RxNormMapping()
        {

        }


        public RxNormMapping(string map)
        {

            string[] fields = null;

            try
            {
                fields = map.Split('|');

                Uuid = Guid.Parse(fields[0]);
                RxCui.Add(int.Parse(fields[2]));
                Description = fields[3].Trim();

            }
            catch { }

        }

        public static RxNormMapping FromMapping(string map)
        {
            return new RxNormMapping(map);
        }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
