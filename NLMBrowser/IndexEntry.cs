﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace NLMBrowser
{
    public class IndexEntry : INotifyPropertyChanged
    {

        public static List<KeyValuePair<string, ZipArchive>> SourceCache { get; protected set; } = new List<KeyValuePair<string, ZipArchive>>();

        public static List<IndexEntry> SourceIndex { get; set; } = new List<IndexEntry>();

        private Guid id;
        private List<int> rxcui = new List<int>();
        private string xml;
        private DateTime timestamp;
        private ObservableCollection<ImageInfo> images = new ObservableCollection<ImageInfo>();

        private string sourcefile;
        private string idfile;

        public Guid Id
        {
            get => id;
            set
            {
                if (id == value) return;
                id = value;
                OnPropertyChanged();
            }
        }

        public string RxCuiList
        {
            get 
            {
                string s = "";
                foreach (var rx in rxcui)
                {
                    if (s != "") s += ",";
                    s += rx.ToString();
                }

                return s;
            }
        }

        public List<int> RxCui
        {
            get => rxcui;
            set
            {
                if (rxcui == value) return;
                rxcui = value;
                OnPropertyChanged();
                OnPropertyChanged("RxCuiList");
            }
        }
        public string Xml
        {
            get => xml;
            set
            {
                if (xml == value) return;
                xml = value;
                OnPropertyChanged();
            }
        }

        public string SourceFile 
        {
            get => sourcefile;
            set
            {
                if (sourcefile == value) return;
                sourcefile = value;
                OnPropertyChanged();
            }
        }

        public string IdFile
        {
            get => idfile;
            set
            {
                if (idfile == value) return;
                idfile = value;
                OnPropertyChanged();
            }
        }

        public DateTime Timestamp
        {
            get => timestamp;
            set
            {
                if (timestamp == value) return;
                timestamp = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ImageInfo> Images
        {
            get => images;
            internal set
            {
                images = value;
                OnPropertyChanged();
            }
        }

        protected void Map(int rxcui)
        {
            if (this.rxcui.Contains(rxcui)) return;

            RxCui.Add(rxcui);

            OnPropertyChanged("RxCui");
            OnPropertyChanged("RxCuiList");
        }

        public static int MapRxCui(List<RxNormMapping> mappings, ArchiveHandler.ReadingFileEvent callback = null)
        {
            int mapped = 0;

            var tempmap = new List<RxNormMapping>();
            tempmap.AddRange(mappings.ToArray());

            int i, c = tempmap.Count;
            int orgC = SourceIndex.Count;

            RxNormMapping mp;

            SourceIndex.Sort(new Comparison<IndexEntry>((a, b) =>
            {
                return string.Compare(a.Id.ToString(), b.Id.ToString());

            }));

            tempmap.Sort(new Comparison<RxNormMapping>((a, b) =>
            {
                return string.Compare(a.Uuid.ToString(), b.Uuid.ToString());
            }));

            foreach (var idx in SourceIndex)
            {
                for (i = 0; i < c; i++)
                {
                    mp = tempmap[i];

                    if (mp.Uuid == idx.Id)
                    {
                        idx.RxCui.AddRange(mp.RxCui);

                        mp.IsMapped = true;
                        mp.MappedItem = idx;

                        mapped++;

                        if (mapped % 128 == 0)
                        {
                            App.Current.Dispatcher.Invoke(() => 
                            {
                                callback?.Invoke(null, new ReadingFileEventArgs("Mapping", "Mapping", mapped, orgC));
                            });
                        }

                        c--;
                        tempmap.RemoveAt(i);

                        break;
                    }
                }
            }

            return mapped;
        }

        public static ZipArchive GetZipSource(string path)
        {
            foreach (var v in SourceCache)
            {
                if (v.Key == path)
                {
                    return v.Value;
                }
            }

            var zip = ZipFile.OpenRead(path);
            var zOut = new KeyValuePair<string, ZipArchive>(path, zip);
            SourceCache.Add(zOut);

            return zOut.Value;
        }

        public static List<IndexEntry> IndexZipSource(string path)
        {
            var zip = GetZipSource(path);
            List<IndexEntry> list = new List<IndexEntry>();

            foreach (var e in zip.Entries)
            {
                var b = false;
                var f = Path.GetFileName(e.FullName);

                string[] fparts = f.Split('_');
                string guidPart = fparts[1].ToLower().Replace(".zip", "");

                Guid g = Guid.Empty;

                if (!Guid.TryParse(guidPart, out g))
                {
                    g = Guid.Parse(guidPart.Substring(1));
                }

                var y = int.Parse(fparts[0].Substring(0, 4));
                var m = int.Parse(fparts[0].Substring(4, 2));
                var d = int.Parse(fparts[0].Substring(6, 2));

                foreach (var check in SourceIndex)
                {
                    if (check.Id == g)
                    {
                        b = true;
                        break;
                    }
                }

                if (b == true) continue;

                list.Add(new IndexEntry(g, path) { IdFile = e.FullName, Timestamp = new DateTime(y, m, d) });
            }

            SourceIndex.AddRange(list);
            return list;
        }

        public IndexEntry(Guid id, string sourceFile)
        {
            if (!File.Exists(sourceFile))
                throw new FileNotFoundException();

            SourceFile = sourceFile;
            Id = id;
        }

        public void Read(bool excludeImages = false)
        {
            var zip = GetZipSource(SourceFile);
            var zName = IdFile;
            var entry = zip.GetEntry(zName);
            var st = entry.Open();

            int blen = (int)entry.Length;
            byte[] buffer = new byte[blen];

            st.Read(buffer, 0, blen);
            st.Close();

            var mst = new MemoryStream(buffer);

            var zip2 = new ZipArchive(mst);

            foreach (var entry2 in zip2.Entries)
            {
                var ext = Path.GetExtension(entry2.FullName).ToLower();
                var estream = entry2.Open();

                switch (ext)
                {
                    case ".jpg":
                    case ".bmp":
                    case ".png":
                    case ".gif":

                        if (excludeImages) break;

                        IntPtr ip = IntPtr.Zero;
                        Images.Add(new ImageInfo() { Name = entry2.Name, Image = DataTools.Interop.Desktop.Resources.MakeWPFImage((Bitmap)Bitmap.FromStream(estream), ref ip) });

                        break;

                    case ".xml":

                        byte[] sbuff = new byte[entry2.Length];

                        estream.Read(sbuff, 0, sbuff.Length);
                        estream.Close();

                        Xml = Encoding.UTF8.GetString(sbuff);

                        break;

                    default:
                        break;

                }

                estream.Close();
            }

            zip2.Dispose();
            mst.Dispose();

        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
