﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Forms;
using SqlLib;

namespace NLMBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Config config = Cache.ReadConfig();

        private ArchiveHandler arch = null;
        private bool cancel = false;

        private bool sessionLoaded = false;

        public MainWindow()
        {
            InitializeComponent();
            btnBegin.IsEnabled = false;

            arch = new ArchiveHandler();
            arch.ReadingFile += Arch_ReadingFile;

         
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (!sessionLoaded) LoadSession();
        }

        private Task LoadSession() 
        {

            var tsk = new Task(() =>
            {
                try
                {

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        sessionLoaded = true;
                    });

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        Title.Content = "Loading previous session...";
                        txtDir.Text = config.DataPath;
                    });

                    IndexEntry.SourceIndex = Cache.ReadIndexFile();
                    var rx = Cache.ReadRxNormFile();

                    foreach (var t in config.ZipFiles)
                    {
                        IndexEntry.GetZipSource(t);
                    }

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        lvIndex.ItemsSource = rx;
                        Title.Content = "Ready";
                    });

                    IndexEntry.MapRxCui(rx, Arch_ReadingFile);

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        Title.Content = "Ready";
                        prog.Value = 0;
                        Message.Content = "";
                        Status.Content = "";
                        sessionLoaded = true;
                    });

                }
                catch
                {

                }

            });

            tsk.Start();
            return tsk;
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();

            dlg.Description = "Select the directory that contains only NLM zip files and indices.";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtDir.Text = dlg.SelectedPath;
            }
            else
            {
                return;
            }

        }

        private void Arch_ReadingFile(object sender, ReadingFileEventArgs e)
        {
            Title.Content = e.Title;
            Message.Content = e.Message;

            if (cancel)
            {
                e.Cancel = true;

                btnBegin.Content = "B_egin";
                Status.Content = "";
                Message.Content = "";
                Title.Content = "Task Cancelled";
                return;
            }

            if (e.State == ReadingFileState.Cancelled)
            {
                Title.Content = "Task Cancelled";
                btnBegin.Content = "B_egin";

                prog.Maximum = 100;
                prog.Minimum = 0;
                prog.Value = 0;

                lvIndex.ItemsSource = arch.RxIndices;

                return;
            }
            else if (e.State == ReadingFileState.Finished)
            {
                prog.Maximum = 100;
                prog.Minimum = 0;
                prog.Value = 0;
                Title.Content = "Ready";
                Status.Content = "";
                Message.Content = "";
                btnBegin.Content = "B_egin";

                lvIndex.ItemsSource = arch.RxIndices;
            }

            else if (e.State == ReadingFileState.Starting)
            {
                prog.Maximum = e.Count;
                prog.Minimum = 0;
                prog.Value = 0;
                cancel = false;
                Title.Content = "Starting ...";
            }
            else
            {
                prog.Value = e.Progress;
                if (prog.Maximum != e.Count) prog.Maximum = e.Count;

                Status.Content = $"{e.Progress} of {e.Count} ...";
            }
        }

        private void btnBegin_Click(object sender, RoutedEventArgs e)
        {
            if ((string)btnBegin.Content == "_Parse")
            {
                cancel = false;
                btnBegin.Content = "_Cancel";

                Task t = arch.ReadRxNormIndices().ContinueWith((lp) =>
                {
                    if (cancel) return;
                    App.Current.Dispatcher.Invoke(() =>
                    {
                        btnBegin.Content = "_Cancel";
                    });

                    arch.ScanDir();

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        Title.Content = "Mapping RxCui...";
                    });

                    List<RxNormMapping> newl = new List<RxNormMapping>();

                    arch.RxIndices.Sort(new Comparison<RxNormMapping>((a, b) =>
                    {
                        int c = string.Compare(a.Uuid.ToString(), b.Uuid.ToString());
                        if (c == 0)
                        {
                            c = string.Compare(a.Description, b.Description);
                        }

                        return c;
                    }));

                    Guid lg = Guid.Empty;
                    //int lx = 0;

                    foreach (var i in arch.RxIndices)
                    {
                        //if (lg == i.Uuid && i.RxCui.Contains(lx))
                        //    continue;

                        //lg = i.Uuid;
                        //lx = i.RxCui.LastOrDefault();

                        newl.Add(i);
                    }

                    arch.RxIndices = newl;

                    IndexEntry.MapRxCui(newl, Arch_ReadingFile);

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        for (int j = newl.Count - 1; j >= 0; j--)
                        {
                            if (newl[j].IsMapped == false) newl.RemoveAt(j);
                        }

                        arch.RxIndices = newl;
                        lvIndex.ItemsSource = newl;

                        Title.Content = "Done.";
                        btnBegin.Content = "_Parse";
                    });

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        config.DataPath = txtDir.Text;

                    });
                    var l = new List<string>();

                    foreach (var c in IndexEntry.SourceCache)
                    {
                        l.Add(c.Key);
                    }

                    config.ZipFiles = l;

                    Cache.WriteConfigFile(config);
                    Cache.WriteIndexFile(IndexEntry.SourceIndex);
                    Cache.WriteRxNormFile(newl);

                    App.Current.Dispatcher.Invoke(() =>
                    {
                        Title.Content = "Done.";
                        Status.Content = "";
                        Message.Content = "";

                        prog.Value = 0;

                        btnBegin.Content = "B_egin";
                    });
                });
            }
            else if ((string)btnBegin.Content == "_Cancel")
            {
                cancel = true;
            }
        }

        private void lvIndex_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void txtDir_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.IO.Directory.Exists(txtDir.Text))
            {
                arch.SetPath(txtDir.Text);
                btnBegin.IsEnabled = true;
            }
            else
            {
                arch.SetPath(null);
                btnBegin.IsEnabled = false;
            }

        }

        private void lvIndex_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var sl = lvIndex.SelectedItem as RxNormMapping;
                if (sl == null) return;

                var info = new ItemDetails(sl.MappedItem);
                info.ShowDialog();

            }
            catch { }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Cache.ClearIndexFile();
            Cache.ClearConfigFile();

            arch.RxIndices.Clear();
            lvIndex.ItemsSource = arch.RxIndices;

            txtDir.Text = Cache.CacheDirectory;
            config.ZipFiles.Clear();
            config.DataPath = Cache.CacheDirectory;

            Cache.WriteConfigFile(config);
            Cache.LastPath = txtDir.Text;

        }

        bool aborted = false;
        bool processing = false;

        private void btnStartSQL_Click(object sender, RoutedEventArgs e)
        {

            if (processing)
            {
                aborted = true;
                return;
            }
            
            Task t = new Task(() =>
            {
                //var lnk = new LinkInterface();
                int i = 0;
                var Index = lvIndex.ItemsSource as List<RxNormMapping>;

                int c = Index.Count;
                //if (!lnk.OpenLink())
                //{
                //    return;
                //}

                Dispatcher.Invoke(() =>
                {
                    prog.Minimum = 0;
                    prog.Maximum = c;
                    prog.Value = 0;
                    aborted = false;
                    processing = true;

                    Status.Content = $"0 / {c} Records";
                    Title.Content = "Populating MySQL Database";
                    btnStartSQL.Content = "_Cancel";

                });


                foreach (RxNormMapping sl in Index)
                {
                    if (aborted) break;

                    InsertRecords(sl/*, lnk */);
                    i++;

                    Dispatcher.Invoke(() =>
                    {
                        prog.Value = i;
                        Status.Content = $"{i} / {c} Records";

                        GC.Collect(0);
                    });

                }

                // lnk.CloseLink();

                Dispatcher.Invoke(() =>
                {
                    prog.Minimum = 0;
                    prog.Maximum = 0;
                    prog.Value = 0;

                    aborted = processing = false;

                    Status.Content = "";
                    Title.Content = $"Done with {c} Records";
                    btnStartSQL.Content = "_Push To SQL";

                });


            });

            t.Start();

        }

        private void InsertRecords(RxNormMapping sl, LinkInterface lnk = null)
        {

            if (sl == null) return;

            bool ownLink;

            if (lnk == null)
            {
                ownLink = true;

                lnk = new LinkInterface();
                lnk.OpenLink();
            }
            else
            {
                ownLink = false;
            }

            var Entry = sl.MappedItem;

            if (Entry.Images.Count > 0)
            {
                Dispatcher.Invoke(() =>
                {
                    Entry.Images.Clear();
                });
            }

            Entry.Read(true);

            //XMLArea.Text = Entry.Xml;

            var drug = Drug.FromXml(Entry.Xml, Entry.Id);
            var recs = drug.GetDosages();

            int idx;
            int doseIdx = 0;

            if (lnk.IsLinkOpen)
            {
                // create the RxCUI Mapping for the data set

                var dsid = sl.Uuid;

                var xmlRec = new MySqlRecord();

                xmlRec.Add("guid", Entry.Id.ToString("D"));
                xmlRec.Add("xml", Entry.Xml);

                lnk.InsertRow("xml_ref", xmlRec);

                foreach (int rx in sl.RxCui)
                {
                    var rxrec = new MySqlRecord();

                    rxrec.Add("guid", dsid.ToString("D"));
                    rxrec.Add("rxcui", rx.ToString());

                    lnk.InsertRow("rxcui_map", rxrec);
                }

                foreach (var rec in recs)
                {
                    idx = lnk.InsertRow("drug_info", rec);

                    if (idx > 0)
                    {
                        var ings = drug.GetIngredients(doseIdx++, idx);
                        foreach (var ing in ings)
                        {
                            lnk.InsertRow("ingredients", ing);
                        }
                    }
                }

            }

            if (ownLink) lnk.CloseLink();

            if (Entry.Images.Count > 0)
            {
                Dispatcher.Invoke(() =>
                {
                    Entry.Images.Clear();
                });
            }

            Entry.Xml = null;
            
            Entry = null;


        }


    }
}
